using UnityEngine;
using System.Collections;

public static class GameEvents
{
	public static string Begin_Play = "Begin Play";
	public static string Main_Menu_Play = "Enter Game";
	public static string Restart = "Restart";
	public static string End_The_Game = "Game Over";

}
