﻿using UnityEngine;
using System.Collections;

public class EnemyEvents : MonoBehaviour
{

	public const string Enemy_State_Idle = "Enemy_Idle";
	public const string Enemy_State_Moving = "Enemy_Moving";
	public const string Enemy_State_Tracking = "Enemy_Tracking";
	public const string Enemy_State_Attacking = "Enemy_Attacking";
	
}
